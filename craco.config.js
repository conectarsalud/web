const CracoAntDesignPlugin = require("craco-antd");

module.exports = {
  plugins: [
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeTheme: {
          // Green: '#389e0d'
          '@primary-color': '#389e0d',
          '@primary-1':'#bae7ff',
          // Purple: '#b37feb'
          '@layout-header-background': '#b37feb',
          '@background-color-base': '#b37feb',
          '@menu-bg':'#b37feb',
          '@layout-trigger-color': '#b37feb',
          '@layout-header-padding': '0 0',
          '@layout-header-height': '90px', // major text color
          '@text-color': '#000',
          '@layout-body-background': '#fff',
          '@menu-highlight-color':'#000',

        }
      }
    }
  ]
};
