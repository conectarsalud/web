import React from "react";
import './App.css';
import { Layout } from 'antd';
import LoginPage from "./login/LoginPage"
import LoginHeader from "./login/LoginHeader"
import MainHeader from "./shared/MainHeader"
import ProffesionalPage from "./proffesional/ProffesionalPage"
import AdminPage from "./admin/AdminPage"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function PrivateRoute({ children, ...rest }) {
  const token = localStorage.getItem('token');
  return (
    <Route
      {...rest}
      render={({ location }) =>
        //Checks if token exists
        token ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

const { Header } = Layout;

function App() {
  return (
    <Router>
      <div className="App">

        <Switch>
          <Route exact path="/">
            <Header>
              <LoginHeader/>
            </Header>
              <LoginPage/>
          </Route>

          <PrivateRoute path="/profesional">
            <Header>
              <MainHeader/>
            </Header>
              <ProffesionalPage/>
          </PrivateRoute>

          <PrivateRoute path ="/admin">
            <Header>
              <MainHeader/>
            </Header>
              <AdminPage/>
          </PrivateRoute>
      </Switch>

    </div>
  </Router>
  );
}

export default App;
