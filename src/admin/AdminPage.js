import React from "react";
import {
  Route,
  useRouteMatch,
  Redirect
} from "react-router-dom";
import { Layout } from 'antd';
import Proffesionals from "./Proffesionals"
import InDevelopmentContent from "../shared/InDevelopmentContent"
import AdminSideMenu from "./AdminSideMenu"
import StatisticsPage from "./StatisticsPage"


const { Sider, Content } = Layout;

export default function AdminPage(){

  let { path } = useRouteMatch();

  const ADMIN_PATHS =
  {
    proffesionals: path + "/profesionales",
    myUser: path + "/mi_usuario",
    statistics: path + "/estadisticas"
  }

  return(
    <Layout style={{minHeight:'86vh'}}>

      <Sider width='15%' className="site-layout-background">
        <AdminSideMenu admin_paths = {ADMIN_PATHS} />
      </Sider>

      <Layout style={{minHeight:'100%'}}>
        <Content style={{ padding: 24,}}>

          <Route exact path={path}>
            <Redirect to={{ pathname: ADMIN_PATHS["proffesionals"]}}/>
          </Route>

          <Route exact path={ADMIN_PATHS["statistics"]}>
              <StatisticsPage/>
          </Route>

          <Route exact path={ADMIN_PATHS["myUser"]}>
              <InDevelopmentContent/>
          </Route>

          <Route exact path={ADMIN_PATHS["proffesionals"]}>
              <Proffesionals/>
          </Route>
        </Content>
      </Layout>
    </Layout>
  )
}
