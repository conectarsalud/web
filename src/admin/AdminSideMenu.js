import React from "react";
import {Menu} from 'antd';
import { Link } from "react-router-dom"

export default function AdminSideMenu({admin_paths}){
  
  function clearToken(){
    localStorage.clear()
  }

  return(
    <Menu mode="vertical" defaultSelectedKeys={['2']}>
      <Menu.Item key="1">
        Mi usuario
        <Link to= {admin_paths["myUser"]}/>
      </Menu.Item>
      <Menu.Item key="2">
        Gestión de profesionales
        <Link to= {admin_paths["proffesionals"]}/>
      </Menu.Item>
      <Menu.Item key="3">
        Estadísticas
        <Link to= {admin_paths["statistics"]}/>
      </Menu.Item>
      <Menu.Item key="4">
        Logout
        <Link to= "/" onClick={clearToken}/>
      </Menu.Item>
    </Menu>
  )
}
