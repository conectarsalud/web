import React, { useState, useEffect } from "react";
import axios from "axios";
import Table from "../shared/Table.js"
import { Typography } from 'antd';
const { Title } = Typography;

export default function Proffesionals(){
  const [data, setData] = useState(null);

  const DOCTORS_URL = "https://conectar-salud-backend.herokuapp.com/api/doctors"
  const COLUMN_TO_FILTER = "nombre"
  const FILTER_TEXT = "Buscar profesional"
  const ADD_PROFFESIONAL_TEXT = "Agregar profesional"


  const day_dictionary = {
    1: "Lunes",
    2: "Martes",
    3: "Miércoles",
    4: "Jueves",
    5: "Viernes"
  }

  const columns =
   [
         {
           Header: "Nombre",
           accessor: "nombre",
           filter: "noDiacriticSearch",
           width: "20%"
         },
         {
           Header: "Matrícula",
           accessor: "matricula",
           width: "10%"
         },
         {
           Header: "Especialidades",
           accessor: "especialidades",
           width: "25%"
         },
         {
           Header: "Horario de atención",
           accessor: "horario_atencion",
           width: "35%",
           Cell: ({ cell: { value } }) => {
             var text = value.map(day_opening_time =>
               day_dictionary[day_opening_time.dia] + " : " +
               day_opening_time.hora_inicio+
               "-"+
               day_opening_time.hora_fin+
               "hs \xa0\xa0"
             )

               return (
                 <>
                   {text}
                 </>
               );
             }
         },
         {
           Header: "Disponible",
           accessor: "disponible",
           width: "10%"
         }
   ]

  useEffect(
    () => {
      (async () => {
        const result = await axios(DOCTORS_URL);
        setData(result.data.doctors);
      })
      ();
    },
    []
  );

  return (
    <>
      <Title level={3}> Servicio de administración de profesionales </Title>
      {data !== null &&
        <Table columns={columns} data={data}
               columnToFilter={COLUMN_TO_FILTER} filterText ={FILTER_TEXT}
               topRightButtonText={ADD_PROFFESIONAL_TEXT} />

      }
    </>
  );
}
