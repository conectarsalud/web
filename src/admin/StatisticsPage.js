import React, { useState, useEffect } from "react";
import axios from "axios";
import { Typography } from 'antd';
import { Line } from '@ant-design/charts';
import { Space, DatePicker,Button, Select } from 'antd';
import moment from 'moment';

const { Title } = Typography;
const { RangePicker } = DatePicker;
const { Option } = Select;

export default function StatisticsPage(){

  const ALL_SPECIALITIES_KEY = -1
  const ALL_SPECIALITIES = {
    "medical_specialty_id": ALL_SPECIALITIES_KEY,
    "name": "Todas las especialidades"}

  const ALL_PROFFESIONALS_KEY = -1
  const ALL_PROFFESIONALS = {
    "doctor_id": ALL_PROFFESIONALS_KEY,
    "nombre": "Todos los medicos"}

  const [data, setData] = useState(null);
  const [dateStart, setDateStart] = useState(moment().subtract(1, 'months'));
  const [dateEnd, setDateEnd] = useState(moment());
  const [specialities, setSpecialities] = useState(null);
  const [selectedSpecialityId, setSelectedSpecialityId] = useState(ALL_SPECIALITIES_KEY);
  const [proffesionals, setProffesionals] = useState(null);
  const [selectedProffesionalId, setSelectedProffesionalId] = useState(ALL_PROFFESIONALS_KEY);

  const PROFFESIONALS_URL = "https://conectar-salud-backend.herokuapp.com/api/doctors"
  const STATISTICS_URL =
    "https://conectar-salud-backend.herokuapp.com/api/stats/daily_ratings"
  const SPECIALITIES_URL =
    "https://conectar-salud-backend.herokuapp.com/api/stats/medical_specialties"

  function selectSpeciality(id){
    console.log("Id spec", id);
    setSelectedSpecialityId(id);
  }

  function selectProffesional(id){
    console.log("Id pro", id);
    setSelectedProffesionalId(id);
  }

  function setDateRange(dates){
    setDateStart(dates[0]);
    setDateEnd(dates[1]);
  }

  async function updatePlot(){
    const fromString =
      dateStart.format('YYYY-MM-DD');
    const toString =
      dateEnd.format('YYYY-MM-DD');

    var parameters = {params: {
      from: fromString, to: toString
    }}

    if(selectedSpecialityId !== ALL_SPECIALITIES_KEY){
      parameters["params"]["medical_specialty_id"] = selectedSpecialityId;
    }

    if(selectedProffesionalId !== ALL_PROFFESIONALS_KEY){
      parameters["params"]["doctor_id"] = selectedProffesionalId;
    }

    console.log("Parameters: ", parameters);

    const result = await axios.get(STATISTICS_URL, parameters);
    console.log("Update request: ", result);
    setData(
      result.data.daily_ratings.map(
        x =>
          {
            return (
              x.valor != null ? {fecha: x.fecha, valor: parseFloat(x.valor)}
              : {fecha: x.fecha, valor: null})
          }
      )
    );
    return;
  }

  async function loadSpecialities(){
    const result = await axios.get(SPECIALITIES_URL);
    var specialities = result.data.medical_specialties;
    specialities.unshift(ALL_SPECIALITIES)
    setSpecialities(specialities);
  }

  async function loadProffesionals(){
    const result = await axios.get(PROFFESIONALS_URL);
    var proffesionals = result.data.doctors;
    proffesionals.unshift(ALL_PROFFESIONALS);
    setProffesionals(proffesionals);
  }

  function firstLoad(){
    updatePlot();
    loadSpecialities();
    loadProffesionals();
  }

  useEffect(firstLoad, []);


 const config = {
   data,
   title: {
     visible: true,
     text: 'Promedio puntuaje por día',
   },
   smooth: 'true',
   xField: 'fecha',
   yField: 'valor',
   point: {
     visible: true,
     size: 5,
     shape: 'circle',
     style: {
       fill: 'white',
       stroke: '#2593fc',
       lineWidth: 2,
     },
   },
   xAxis:{
     visible: true,
     tickCount: 10,
     title: {
       visible: true,
       text: 'Día',
     }
   },
   yAxis:{
     visible: true,
     title: {
       visible: true,
       text: 'Puntaje promedio',
     },
   },
   tooltip:{
     visible: true,
     formatter:(x, y)=>{
       var tooltipText ;
       if(y != null){
         tooltipText = {name:'Puntuaje', value:y};
       }else{
         tooltipText = {name:'No hay datos'};
       }
       return(tooltipText);
     }
   }
  };

  return (
    <>
      <Space direction="vertical">
      <Title level={3}> Estadísticas </Title>
      <Space>
      <div>
        <RangePicker
          defaultValue={[
            dateStart, dateEnd
          ]}
          disabledDate={current => {
            return current < moment().subtract(3, 'months') ||
                   current > moment();
          }}
          onChange={setDateRange}
        />
      </div>
      <div>
        <Select onSelect={selectSpeciality} style={{width: "18vw"}}
          defaultValue="Todas las especialidades">
          {specialities != null &&
            specialities.map(speciality =>
              <Option
              value={speciality.medical_specialty_id}>{speciality.name}</Option>)
          }
        </Select>
      </div>
      <div>
      <Select style={{width: "18vw"}}
        onSelect={selectProffesional} defaultValue="Todos los profesionales">
        {proffesionals != null &&
          proffesionals.map(proffesional =>
            <Option
            value={proffesional.doctor_id}>{proffesional.nombre}</Option>)
        }
      </Select>
    </div>
      <Button  style={{width: "15vw"}}
      onClick={updatePlot}>Actualizar gráfico</Button>
      </Space>
      {data !== null &&
        <Line {...config} />
      }
      </Space>
    </>

  );
}
