import React, { useState } from "react";
import axios from "axios"

import {
  useHistory
} from "react-router-dom";

import { Form, Input, Button, Card, Alert } from 'antd';

const itemLayout = null;
const buttonItemLayout=null;
const formLayout='vertical';

function Login(props){
  const history = useHistory()
  const [failedLogin, setFailedLogin] = useState(false);
  const onFinish = values => {
    if(values.username === "Admin" || values.username === "admin"){
      localStorage.setItem('token', "fakeToken");
      history.push("/admin")
    }else{
      const LOGIN_URL = "https://conectar-salud-backend.herokuapp.com/api/doctor/login";
      const username = values.username;
      const password = values.password;

      axios.post(LOGIN_URL, { username, password })
        .then(res => {
          console.log("Token: ", res.data.token.token);
          localStorage.setItem('token', res.data.token.token);
          history.push("/profesional")
        })
        .catch((reason: AxiosError) => {
          console.log(reason)
          if(reason.response.code !== 200){
            console.log("Error response:", reason.response);
            console.log("Error code:", reason.response.data["code"]);
            console.log("Error text:", reason.response.data["message"]);
          }
          if(reason.response.data.code === 401){
            setFailedLogin(true);
          }
        })
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div style={{display: 'flex', justifyContent: 'center', paddingTop: '2%'}}>
      <Card title="Inicia sesión con tu cuenta" style={{ width: '20%', textAlign: 'center'}}>
        <Form
          {...itemLayout}
          layout={formLayout}
          name="basic"
          initialValues={{
            size: 'small'
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Usuario"
            name="username"
            rules={[
              {
                required: true,
                message: 'Por favor ingresa tu nombre de usuario',
              },
            ]}
          >
          <Input />
          </Form.Item>

            <Form.Item
              label="Contraseña"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Por favor ingresa tu contraseña',
                },
              ]}
            >
            <Input.Password />
          </Form.Item>
          {failedLogin &&
            <Alert style={{marginBottom: "20px"}} message="Credenciales inválidas" type="error" />
          }
          <Form.Item {...buttonItemLayout}>
            <Button type="primary" htmlType="submit">
              Iniciar sesión
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

export default Login
