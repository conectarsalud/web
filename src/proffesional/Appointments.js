import React, { useState, useEffect } from "react";
import axios from "axios";
import Table from "../shared/Table.js"
import { Typography } from 'antd';
const { Title } = Typography;

export default function Appointments(){

  const [data, setData] = useState(null);
  const APPOINTMENTS_URL = "https://conectar-salud-backend.herokuapp.com/api/doctor/appointments"

  const columns =
   [
         {
           Header: "Fecha",
           accessor: "hora_solicitud",
           width: "10%",
           Cell: ({ cell: { value } }) => {
             const text = value.slice(0,10)
               return (
                 <>
                   {text}
                 </>
               );
             }
         },
         {
           Header: "Paciente",
           accessor: "member.nombre",
           width: "20%"
         },
         {
           Header: "Especialidad",
           accessor: "especialidad",
           width: "10%"
         },
         {
           Header: "Motivo",
           accessor: "motivo",
           width: "30%"
         },
         {
           Header: "Indicaciones",
           accessor: "informacion.indicaciones",
           width: "30%"
         }
   ]

   useEffect(
     () => {
       (async () => {
         const header =
           {headers:
             {"Authorization" : "Bearer " + localStorage.getItem("token")}
           };

         const result = await axios(APPOINTMENTS_URL, header);
         const appointments = result.data.appointments;
         setData(appointments);
       })
       ();
     },
     []
   );


   return (
     <>
     <Title level={3} style={{textAlign: "center"}}>Mis consultas</Title>
     {data !== null &&
       <Table columns={columns} data={data}
       />
     }
     </>
   );
}
