import React, { useState } from "react";
import { Row, Col, Space,Typography, Input, Button, Form, Upload, message, Modal} from 'antd';
import { useHistory } from "react-router-dom"
import { UploadOutlined } from '@ant-design/icons';
import axios from "axios"
const { Title } = Typography;
const { TextArea } = Input;

export default function PatientCallContent(props){
  const [prescriptionList, setprescriptionList] = useState([]);
  const [indications, setIndications] = useState();

  //The whole modal logic needs a refactor
  //But client hasn't confirm it if he wants the features, so we are delaying it
  const [modalVisible, setModalVisible] = useState(false);

  const itemLayout = null;
  const buttonItemLayout=null;
  const formLayout='vertical';
  const history = useHistory()
  const patientName = history.location.state.patientName;
  const speciality = history.location.state.speciality;
  const cause = history.location.state.cause;
  const date = history.location.state.date;
  const qr = history.location.state.qr;
  const appointment_id = history.location.state.appointment_id;
  message.config({
    maxCount: 1
  });

  console.log("id: ", appointment_id)





  function uploadPrescription(aPrescription){
    const UPLOAD_URL =
      `https://conectar-salud-backend.herokuapp.com/api/doctor/appointment/${appointment_id}/upload_data`;

    const header = {
      headers:
        {
          "Authorization" : "Bearer " + localStorage.getItem("token"),
          "Content-Type": "multipart/form-data"
        }
    };

    let formData = new FormData();
    formData.append("indications", indications);
    formData.append("prescription", aPrescription);
    axios.post(UPLOAD_URL, formData, header)
      .then(res => {
        console.log("Upload succesfully")
        message.success('Indicaciones cargadas');
      })
      .catch((reason) => {
        console.log("Reason: ", reason)
        console.log("Srtingified Reason:", JSON.stringify(reason));
        message.error('No atendiste la consulta');
      })

  }

  function uploadIndications(){

    const UPLOAD_URL =
      `https://conectar-salud-backend.herokuapp.com/api/doctor/appointment/${appointment_id}/data`;

    const header = {
      headers:
        {
          "Authorization" : "Bearer " + localStorage.getItem("token")
        }
    };

    const body = {
      "indications":  indications
    }

    axios.post(UPLOAD_URL, body, header)
      .then(res => {
        console.log("Upload succesfully")
        message.success('Indicaciones cargadas');
      })
      .catch((reason) => {
        console.log("Reason: ", reason)
        console.log("Srtingified Reason:", JSON.stringify(reason));
        message.error('No atendiste la consulta');
      })
  }

  function uploadData(){
    console.log("Indications:", indications);
    console.log("prescriptions: ", prescriptionList);
    uploadIndications();
    prescriptionList.map(
      (prescription) => uploadPrescription(prescription)
    );
  }

  function showConfirm(values){
    setIndications(values.indications)
    setModalVisible(true);
  }

  function hideModal(){
    setModalVisible(false);
  }

  const uploadProps = {
    listType: 'text',
    className: 'upload-list-inline',
    onRemove: file => {
      console.log("Prescription to remove: ", file);
      console.log("Prescription List Before Remove: ",prescriptionList)
      setprescriptionList(prescriptionList.filter( item => item.uid !== file.uid));
      console.log("Prescription list: ", prescriptionList)
    },
    beforeUpload: file => {
      setprescriptionList([...prescriptionList, file]);
      console.log("Prescription list: ", prescriptionList)
      return false;
    },
    prescriptionList,
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  }

  let indicationsForModal = null
  if(indications != null){
    console.log("Indications:", indications)
    indicationsForModal = indications.split('\n').map((item, i) => {
      return <p key={i}>{item}</p>;
    });
  }

    return(
      <>
      <Row gutter={[16, 24]}>
        <Col span={24}align="middle">
          <Title level={3}>Atendé tu próxima consulta</Title>
        </Col>

        <Col span={12} className="gutter-row" align="middle">
          <Space direction="vertical">
            <Title level={4}>Datos de la consulta</Title>
            <txt>
              Paciente: {patientName}
            </txt>
            <txt>
              Especialidad solicitada: {speciality}
            </txt>
            <txt>
              Motivo: {cause}
            </txt>
            <txt>
              Fecha de solicitud: {date}
            </txt>
            <img src={qr} style={{height:"35vh"}} alt="QR"></img>

            <txt tyle={{textAlign: 'center'}}>
              Por favor escanee el QR con la aplicación de su celular
            </txt>
          </Space>
        </Col>

        <Col span={12} className="gutter-row" align="middle">
          <Space size="large" direction="vertical" style={{width: "80%"}}>
            <Title level={4}>Indicaciones</Title>
            <Form
              {...itemLayout}
              layout={formLayout}
              name="basic"
              onFinish={showConfirm}
              onFinishFailed={onFinishFailed}
            >

            <Form.Item
              name="indications"
              rules={[
                {
                  required: true,
                  message: 'Por favor ingresa las indicaciones para el paciente',
                },
              ]}
            >
              <TextArea rows={8}/>
            </Form.Item>

            <Form.Item
              name="prescription"
              rules={[
                {
                  required: false
                },
              ]}
            >
              <Upload {...uploadProps} style={{align:"left"}}>
               <Button>
                 <UploadOutlined/> Cargar recetas
               </Button>
              </Upload>,
            </Form.Item>

            <Form.Item {...buttonItemLayout}>
              <Button type="primary" htmlType="submit">Cargar indicaciones</Button>
            </Form.Item>
            <Modal
              title="¿Desea enviarle al paciente las siguientes indicaciones?"
              okText="Si"
              cancelText="No"
              visible={modalVisible}
              onOk={() => { hideModal(); uploadData();}}
              onCancel={hideModal}
            >
              {indicationsForModal}
            </Modal>
            </Form>
          </Space>
        </Col>
      </Row>

      </>
    );
}
