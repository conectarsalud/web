import React from "react";
import {
  Route,
  useRouteMatch,
  Redirect
} from "react-router-dom";
import { Layout } from 'antd';
import ProffesionalSideMenu from "./ProffesionalSideMenu"
import InDevelopmentContent from "../shared/InDevelopmentContent"
import WaitingPatientsContent from "./WaitingPatientsContent"
import PatientCallContent from "./PatientCallContent"
import Appointments from "./Appointments"

const { Sider, Content } = Layout;

export default function ProffesionalPage(){

  let { path } = useRouteMatch();

  const PROFFESIONALS_PATHS =
  {
    appointments: path + "/mis_consultas",
    waitingPatients: path + "/en_espera",
    patientCall: path + "/atender_paciente",
    myUser: path + "/mi_usuario"
  }

  return(
    <Layout style={{minHeight:'86vh'}}>
      <Sider width='15%' className="site-layout-background">
        <ProffesionalSideMenu proffesional_paths = {PROFFESIONALS_PATHS} />
      </Sider>
      <Layout style={{minHeight:'100%'}}>
        <Content style={{ padding: 24,}}>
          <Route exact path={path}>
            <Redirect to={{ pathname: PROFFESIONALS_PATHS["waitingPatients"]}}/>
          </Route>
          <Route exact path={PROFFESIONALS_PATHS["appointments"]}>
            <Appointments/>
          </Route>
          <Route exact path={PROFFESIONALS_PATHS["waitingPatients"]}>
            <Content style={{ padding: 24,}}>
              <WaitingPatientsContent
                patientCallPath = {PROFFESIONALS_PATHS["patientCall"]}
              />
            </Content>
          </Route>
          <Route exact path={PROFFESIONALS_PATHS["patientCall"]}>
            <Content>
              <PatientCallContent/>
            </Content>
          </Route>
          <Route exact path={PROFFESIONALS_PATHS["myUser"]}>
            <Content>
              <InDevelopmentContent/>
            </Content>
          </Route>
        </Content>
      </Layout>
    </Layout>
  )
}
