import React from "react";
import {Menu} from 'antd';
import { Link } from "react-router-dom"

function clearToken(){
  localStorage.clear()
}

export default function ProffesionalSideMenu({proffesional_paths}){
  return(
    <Menu mode="vertical" defaultSelectedKeys={['2']}>
      <Menu.Item key="1">Mi usuario
        <Link to= {proffesional_paths["myUser"]}/>
      </Menu.Item>
      <Menu.Item key="2">
        Afiliados en espera
        <Link to= {proffesional_paths["waitingPatients"]}/>
      </Menu.Item>
      <Menu.Item key="3">
        Mis consultas
        <Link to= {proffesional_paths["appointments"]}/>
      </Menu.Item>
      <Menu.Item key="4">
        Logout
          <Link to= "/" onClick={clearToken}/>
      </Menu.Item>
    </Menu>
  )
}
