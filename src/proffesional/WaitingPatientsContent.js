import React, { useState, useEffect } from "react";
import axios from "axios";
import Table from "../shared/Table.js"
import { Link } from "react-router-dom"
import { Typography } from 'antd';
const { Title } = Typography;

export default function WaitingPatientsContent({patientCallPath}){
  const [data, setData] = useState(null);

  const PATIENTS_URL = "https://conectar-salud-backend.herokuapp.com/api/doctor/queued_appointments"
  const COLUMN_TO_FILTER = "especialidad"
  const FILTER_TEXT = "Buscar especialidad"

  const columns =
   [
         {
           Header: "Nombre",
           accessor: "member.nombre",
           width: "20%"
         },
         {
           Header: "Especialidad",
           accessor: "especialidad",
           filter: "noDiacriticSearch",
           width: "15%"
         },
         {
           Header: "Motivo",
           accessor: "motivo",
           width: "40%"
         },
         {
           Header: "Fecha de la solicitud",
           accessor: "hora_solicitud",
           width: "15%"
         },
         {
           Header: "Acción",
           accessor: "accion",
           width: "10%"
         }
   ]

   //To use when the service is made

  useEffect(
    () => {
      (async () => {

        function attend(row){
          return (<Link to={{pathname: patientCallPath,
                            state: { patientName: row.member.nombre,
                                     speciality: row.especialidad,
                                     cause: row.motivo,
                                     date: row.hora_solicitud,
                                     qr: row.qr_conexion,
                                     appointment_id: row.id
                                      }}  }>Atender</Link>)
        }

        const header =
          {headers:
            {"Authorization" : "Bearer " + localStorage.getItem("token")}
          };

        const result = await axios(PATIENTS_URL, header);
        const appointments = result.data.queued_appointments;
        appointments.map((row) => row["accion"] = attend(row));
        setData(appointments);
      })
      ();
    },
    [patientCallPath]
  );


  return (
    <>
    <Title level={3} style={{textAlign: "center"}}>Afiliados en espera</Title>
    {data !== null &&
      <Table columns={columns} data={data}
             columnToFilter={COLUMN_TO_FILTER} filterText ={FILTER_TEXT}
      />
    }
    </>
  );
}
