import React, { useState} from "react";
import { useTable, useFilters, useSortBy, usePagination } from "react-table";
import "./Table.css";
import NoSearchResult from '../resources/profiles-search-not-found.svg';
import Cogs from '../resources/cogs.svg';

//Button is optional, and a TO DO feature. It was added by a client request.
//It lacks any function
//Use tableStyle to style the CSS to make things fit properly
//Every other field is mandatory
export default function Table({ columns, data,
                                columnToFilter = null,filterText,
                                topRightButtonText = null,
                                tableStyle = null}) {
  const [filterInput, setFilterInput] = useState("");
  // Use the state and functions returned from useTable to build your UI

  const filterTypes = React.useMemo(
    () => ({
      noDiacriticSearch: (rows, id, filterValue) => {
        return rows.filter(row => {
          const rowValue = row.values[id];
          return rowValue !== undefined
            ? String(rowValue.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
                .includes(String(filterValue).toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
            : true;
        });
      }
    }),
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    setFilter,
    pageOptions,
    pageCount,
    gotoPage,
    previousPage,
    nextPage,
    setPageSize,
    canPreviousPage,
    canNextPage,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 5 },
      filterTypes
    },
    useFilters,
    useSortBy,
    usePagination,
  );

  const handleFilterChange = e => {
    const value = e.target.value || undefined;
    setFilter(columnToFilter, value);
    setFilterInput(value);
  };


  // Render the UI for your table
  return (
    <>
      {columnToFilter !== null &&
        <input className="searchInput"
          value={filterInput}
          onChange={handleFilterChange}
          placeholder={filterText}
        />
      }
      {page.length === 0 &&
        <strong className="noResults">
          No se encontraron resultados
        </strong>
      }
      {topRightButtonText !== null &&
        <button className="tableTopRightButton"> {topRightButtonText} </button>
      }
      {/*only display the table if there are elements*/}
      {page.length > 0   ?
        <>
        <table {...getTableProps()} style={tableStyle}>
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th width={column.width}
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    className={
                      column.isSorted
                        ? column.isSortedDesc
                          ? "sort-desc"
                          : "sort-asc"
                        : ""
                    }
                  >
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="pagination">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span  className="tablePageIndex">
            Página{' '}
            <strong>
              {pageIndex + 1} de {pageOptions.length}
            </strong>{' '}
          </span>
          <span className="tableGoToPage">
            | Ir a la página:{' '}
            <input
              type="number"
              defaultValue={pageIndex + 1}
              onChange={e => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
              style={{ width: '100px' }}
            />
          </span>{' '}
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[5, 10, 20, 30, 40, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Mostrar {pageSize}
              </option>
            ))}
          </select>
        </div>
        </>
        :
        <>
        <div>
          <img style={{
            maxWidth: '30%', backgroundColor: 'transparent', paddingTop: '5%'
           }}
           src={Cogs} alt="No search result"
          />
          <img style={{
            maxWidth: '40%', backgroundColor: 'transparent', paddingTop: '5%'
           }}
           src={NoSearchResult} alt="No search result"
          />
          <img style={{
            maxWidth: '30%',backgroundColor: 'transparent', paddingTop: '5%'
           }}
           src={Cogs} alt="No search result"
          />
        </div>
        </>
      }

    </>
  );
}
